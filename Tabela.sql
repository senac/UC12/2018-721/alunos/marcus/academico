create table curso (
id int not null primary key auto_increment ,
nome varchar(400) not null
);

select * from curso ;

select * from disciplina;

alter table disciplina add column id_disciplina int not null;

insert into curso (nome) values ('Informatica');
insert into curso (nome) values ('Logistica');
insert into curso (nome) values ('Administração');
insert into curso (nome) values ('Segurança Do Trabalho');


insert into disciplina (codigo , descricao , id_disciplina) values('TL2' , 'Realizar procedimentos de conferência de equipamentos,materiais e produtos no processo logístico' , 2);
insert into disciplina (codigo , descricao , id_disciplina) values('TL2' , 'Mapear e estimar custos logísticos' , 2);
insert into disciplina (codigo , descricao , id_disciplina) values('TL2' , 'Projeto Integrador em Logística' , 2);
insert into disciplina (codigo , descricao , id_disciplina) values('ADM' , 'Auxiliar a elaboração, implementação e acompanhamento do planejamento estratégico das organizações' , 3);
insert into disciplina (codigo , descricao , id_disciplina) values('ADM' , 'Apoiar e executar ações referentes às rotinas de admissão e demissão de colaboradores' , 3);
insert into disciplina (codigo , descricao , id_disciplina) values('ADM' , 'Projeto Integrador Técnico em Administração' , 3);
insert into disciplina (codigo , descricao , id_disciplina) values('SDT' , 'uxiliar a elaboração, implantação e implementação da Política de Saúde ' , 4);
insert into disciplina (codigo , descricao , id_disciplina) values('SDT' , 'Prestar assistência de primeiros socorros' , 4);
insert into disciplina (codigo , descricao , id_disciplina) values('SDT' , 'Projeto Integrador Técnico em Segurança do Trabalho' , 4);

update disciplina set id_disciplina = 1 where id = 1;
update disciplina set id_disciplina = 1 where id = 2;
update disciplina set id_disciplina = 1 where id = 3;
update disciplina set id_disciplina = 1 where id = 4;
update disciplina set id_disciplina = 1 where id = 5;
update disciplina set id_disciplina = 1 where id = 6;
update disciplina set id_disciplina = 1 where id = 7;
update disciplina set id_disciplina = 1 where id = 8;
update disciplina set id_disciplina = 1 where id = 9;
update disciplina set id_disciplina = 1 where id = 10;
update disciplina set id_disciplina = 1 where id = 11;
update disciplina set id_disciplina = 1 where id = 12;
update disciplina set id_disciplina = 1 where id = 13;
update disciplina set id_disciplina = 1 where id = 14;
update disciplina set id_disciplina = 1 where id = 15;
update disciplina set id_disciplina = 1 where id = 16;

alter table disciplina add constraint fk_curso foreign key (id_disciplina) references curso(id);
